#!/bin/bash -e

DIR=$PWD

yum install -y gcc-c++ gcc make zlib-devel bzip2-devel

#Compile static version of libressl

LIBRESSL=libressl-2.2.5
OPENSSL=openssl-1.0.2f

STATICLIBSSL=/app/libressl
STATICLIBSSL=/app/openssl

HAPROXY=haproxy-1.6.3
PCRE=pcre-8.38

STATICLIBPCRE=/app/pcre

# download 
test -f ${LIBRESSL}.tar.gz || wget -c http://ftp.openbsd.org/pub/OpenBSD/LibreSSL/${LIBRESSL}.tar.gz
test -f ${HAPROXY}.tar.gz || wget -c http://www.haproxy.org/download/1.5/src/${HAPROXY}.tar.gz
test -f ${PCRE}.tar.gz || wget -c ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/${PCRE}.tar.gz


# kernel >= 3.7,support TCP_FAST_OPEN. we support kernel >= 4.0 enable TCP_FAST_OPEN
USE_TFO=''
test `uname -r|awk -F'.' {'print $1'}` -gt '3'  && USE_TFO='USE_TFO=1'


tar zxfv ${OPENSSL}.tar.gz
cd ${OPENSSL}
./config --prefix=$STATICLIBSSL no-shared no-threads && \
make 
test -d ${STATICLIBSSL} ||  sudo make install 
cd - 

tar xvzf ${LIBRESSL}.tar.gz
cd ${LIBRESSL}
./configure  --prefix=$STATICLIBSSL --enable-shared=no
make && make install
cd ${DIR}


tar xvzf ${PCRE}.tar.gz
cd  ${PCRE}
./configure --prefix=$STATICLIBPCRE --enable-shared=no --enable-utf8 --enable-jit \
  --enable-pcre8 --enable-pcre16 --enable-pcre32 --enable-unicode-properties --enable-pcregrep-libz --enable-pcregrep-libbz2 
make && make install

cd ${DIR}


tar xvzf ${HAPROXY}.tar.gz
cd ${HAPROXY}
make clean
make TARGET=linux2628  USE_PCRE_JIT=1 USE_STATIC_PCRE=1 USE_OPENSSL=1 \
     PCRE_LIB=$STATICLIBPCRE/lib PCRE_INC=$STATICLIBPCRE/include  \
     SSL_INC=$STATICLIBSSL/include SSL_LIB=$STATICLIBSSL/lib ADDLIB="-ldl -lrt" USE_ZLIB=1 \
     $USE_TFO 
make install DESTDIR=/app/haproxy PREFIX=""

cd ${DIR}

/app/haproxy/sbin/haproxy -vv


## build rpm package
yum install -y rubygems ruby ruby-devel gcc rpm-build redhat-lsb-core
gem sources --remove https://rubygems.org/
gem sources --remove http://rubygems.org/
gem sources --add https://ruby.taobao.org/
gem install cabin -v 0.7.2 --no-rdoc --no-ri
gem install fpm --no-rdoc --no-ri

# centos 6 ,ruby 1.8.7 ,you need older cabin
test `ruby -v | awk -F' ' {'print $2'}|awk -F'.' {'print $1'}` -lt 2 && \
  gem uninstall cabin --ignore-dependencies --executables && gem install cabin -v 0.7.2 --no-rdoc --no-ri 
HAPROXY_VERSION=`echo $HAPROXY|awk -F"-" {'print $2'}`
rm -rf ${HAPROXY}_*rpm
fpm -s dir -t rpm -n haproxy -v ${HAPROXY_VERSION} -C /app/haproxy \
  -p ${HAPROXY}_$(lsb_release -si)$(lsb_release -sr)_ARCH.rpm --prefix /app/haproxy
rpm -qlp ${HAPROXY}_*_x86_64.rpm